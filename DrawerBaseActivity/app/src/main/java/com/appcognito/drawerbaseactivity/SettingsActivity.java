package com.appcognito.drawerbaseactivity;

import android.os.Bundle;

/**
 * Created by WillKim on 10/04/2017.
 */

public class SettingsActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        super.addContentView(R.layout.settings_activity, "HOME");
    }
}
